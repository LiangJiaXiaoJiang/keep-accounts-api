# KeepAccountsApi

#### 介绍
记账小程序api

#### 软件架构

KeepAccounts.Api 接口

KeepAccounts.Common 帮助

KeepAccounts.Models  实体

KeepAccounts.Repository 数据库访问

KeepAccounts.Services 业务逻辑


#### 开发环境

1.  vs 2022
2.  sql server 2012
3.  dotnet core 7.0

 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

