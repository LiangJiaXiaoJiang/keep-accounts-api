﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using KeepAccounts.Common.Config;
using KeepAccounts.Models;
using KeepAccounts.Models.Models;
using KeepAccounts.Models.Models.UserInfo;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Services.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeepAccounts.Api.Areas.Manager.Controllers
{
    /// <summary>
    /// 用户
    /// </summary>
    [Route("Manager/[controller]")]
    [ApiExplorerSettings(GroupName = Grouping.V2)]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class UserInfoController : Controller
    {

        private readonly IUserInfoServices _userInfoServices;
        /// <summary>
        /// 用户
        /// </summary>
        /// <param name="userInfoServices"></param>
        public UserInfoController(IUserInfoServices userInfoServices)
        {
            _userInfoServices = userInfoServices;
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(FindList))]
        public async Task<ResponseModel> FindList(UserInfoListQueryModel query)
        {
            return ResponseModel.Success(await _userInfoServices.FindList(query));
        }
        /// <summary>
        /// 获取表单信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost(nameof(LoadForm))]
        public async Task<ResponseModel> LoadForm(GuidQueryModel query)
        {
            var model = await _userInfoServices.QueryById(query.Id);
            if (model != null)
            {
                model.HeaderImage = WebConfig.WebSiteUrl + model.HeaderImage;
            }

            return ResponseModel.Success(model == null ? new  UserInfoEntity()
            {
                ID = Guid.Empty, 
            } : model);
        }
        /// <summary>
        /// 添加信息
        /// </summary>
        /// <param name="model">信息</param>
        /// <returns></returns>
        [HttpPost(nameof(Save))]
        public ResponseModel Save(UserInfoEntity model)
        {
            if (_userInfoServices.Save(model))
            {
                return ResponseModel.Success("保存成功");
            }

            return ResponseModel.Error("保存失败");
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="query">ID</param>
        /// <returns></returns>
        [HttpPost(nameof(Delete))]
        public async Task<ResponseModel> Delete(GuidQueryModel query)
        {
            return ResponseModel.Success(await _userInfoServices.DeleteById(query.Id));
        }
    }
}
