﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KeepAccounts.Models;
using KeepAccounts.Models.Swagger;

namespace KeepAccounts.Api.Areas.Manager.Controllers
{
    [Route("Manager/[controller]")]
    [ApiExplorerSettings(GroupName = Grouping.V2)]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class HomeController : ControllerBase
    {

        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Index))]
        public async Task<ResponseModel> Index()
        {

            return ResponseModel.Error("App/Manager/Index");
        }

    }
}