﻿using KeepAccounts.Models;
using KeepAccounts.Models.Models;
using KeepAccounts.Models.Models.BookClass;
using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Repository.BookClass;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeepAccounts.Api.Areas.Manager.Controllers
{

    [Route("Manager/[controller]")]
    [ApiExplorerSettings(GroupName = Grouping.V2)]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class BookClassController : ControllerBase
    {
        private readonly IBookClassServices _bookClassServices;

        public BookClassController(IBookClassServices bookClassServices)
        {
            _bookClassServices = bookClassServices;


        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(FindList))]
        public async Task<ResponseModel> FindList(BookClassListQueryModel query)
        {
            return ResponseModel.Success(await _bookClassServices.FindList(query));
        }


        /// <summary>
        /// 保存/修改
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Save))]
        public ResponseModel Save(BookClassEntity query)
        {
            return ResponseModel.Success(_bookClassServices.Save(query));
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Delete))]
        public async Task<ResponseModel> Delete(GuidQueryModel query)
        {
            return ResponseModel.Success(await _bookClassServices.Delete(query));
        }
    }
}
