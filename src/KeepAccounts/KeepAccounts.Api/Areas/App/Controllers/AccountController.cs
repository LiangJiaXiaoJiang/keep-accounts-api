﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using KeepAccounts.Models;
using KeepAccounts.Models.Models.UserInfo;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Services.Domain;

namespace KeepAccounts.Api.Areas.App.Controllers
{
    /// <summary>
    /// 账户登录
    /// </summary>
    [ApiExplorerSettings(GroupName = Grouping.V3)]
    [Route("App/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        IUserInfoServices _userInfoServices;
        /// <summary>
        /// 
        /// </summary>
        public AccountController(IUserInfoServices userInfoServices)
        {
            _userInfoServices = userInfoServices;

        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(nameof(Login))]
        public async Task<ResponseModel> Login(LoginQueryModel model)
        {
            return ResponseModel.Success(await _userInfoServices.Login(model));
        }


        /// <summary>
        /// 第三方授权登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(nameof(OAuthLogin))]
        public ResponseModel OAuthLogin(OAuthLoginQueryModel model)
        {
            return ResponseModel.Success(_userInfoServices.OAuthLogin(model));
        }


        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(nameof(Register))]
        public async Task<ResponseModel> Register(RegisterQueryModel model)
        {
            return ResponseModel.Success(await _userInfoServices.Register(model));

        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(GetUserInfo))]
        [Authorize]
        public async Task<ResponseModel> GetUserInfo()
        {
            return ResponseModel.Success(await _userInfoServices.GetUserInfo());
        }


        /// <summary>
        /// 推出登陆
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Logout))]
        [Authorize]
        public async Task<ResponseModel> Logout()
        {
            return ResponseModel.Success(await _userInfoServices.Logout());
        }


        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(UpdatePassword))]
        [Authorize]
        public async Task<ResponseModel> UpdatePassword(UpdatePasswordQueryModel query)
        {
            return ResponseModel.Success(await _userInfoServices.UpdatePassword(query));
        }



        /// <summary>
        /// 找回密码
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(FindPassword))]
        public async Task<ResponseModel> FindPassword(FindPasswordQueryModel query)
        {
            return ResponseModel.Success(await _userInfoServices.FindPassword(query));
        }




        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(UpdateUserInfo))]
        [Authorize]
        public async Task<ResponseModel> UpdateUserInfo(UpdateUserInfoQueryModel query)
        {
            return ResponseModel.Success(await _userInfoServices.UpdateUserInfo(query));
        }


        /// <summary>
        /// 刷新token
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(RefreshToken))]
        public async Task<ResponseModel> RefreshToken(string Token)
        {
            return ResponseModel.Success(await _userInfoServices.RefreshToken(Token));
        }


        

    }
}