﻿using KeepAccounts.Models;
using KeepAccounts.Models.Models.BookClass;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Repository.BookClass;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeepAccounts.Api.Areas.App.Controllers
{
    /// <summary>
    /// 分类
    /// </summary>
    [ApiExplorerSettings(GroupName = Grouping.V3)]
    [Route("App/[controller]")]
    [ApiController]
    public class BookClassController : ControllerBase
    {
        private readonly IBookClassServices _bookClassServices;

        public BookClassController(IBookClassServices bookClassServices)
        { 
            _bookClassServices = bookClassServices;
        }

        /// <summary>
        /// 获取分类列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost(nameof(GetBookClassList))]
        public async Task< ResponseModel> GetBookClassList(BookClassListQueryModel query)
        {

            return ResponseModel.Success(await _bookClassServices.GetBookClassList(query));
        }


    }
}
