﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Essensoft.AspNetCore.Payment.Alipay;
using Essensoft.AspNetCore.Payment.Alipay.Notify;
using Essensoft.AspNetCore.Payment.WeChatPay;
using Essensoft.AspNetCore.Payment.WeChatPay.Notify;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using KeepAccounts.Common.Extensions;
using KeepAccounts.Models.Swagger;

namespace KeepAccounts.Api.Areas.App.Controllers
{

    /// <summary>
    /// 支付回调相关
    /// </summary>
    [ApiExplorerSettings(GroupName = Grouping.V3)]
    [Route("App/[controller]")]
    [ApiController]
    public class PayController : ControllerBase
    {

        #region ctor
        private readonly IAlipayNotifyClient _alipayNotifyClient;
        private readonly IOptions<AlipayOptions> _alipayOptionsAccessor;
        private readonly IWeChatPayNotifyClient _weChatPayNotifyClient;
        private readonly IOptions<WeChatPayOptions> _weChatPayOptionsAccessor; 
        public PayController(IAlipayNotifyClient alipayNotifyClient,
            IWeChatPayNotifyClient wxWeChatPayNotifyClient,
            IOptions<AlipayOptions> alipayOptionsAccessor, 
            IOptions<WeChatPayOptions> weChatPayOptionsAccessor)
        {
            _alipayNotifyClient = alipayNotifyClient;
            _alipayOptionsAccessor = alipayOptionsAccessor;
            _weChatPayNotifyClient = wxWeChatPayNotifyClient;
            _weChatPayOptionsAccessor = weChatPayOptionsAccessor; 
        }
        #endregion


        #region 支付成功通知
        /// <summary>
        /// APP支付异步通知
        /// </summary>
        [HttpPost(nameof(AliPayCallBack))]
        public async Task<IActionResult> AliPayCallBack()
        {
            try
            {
                var notify = await _alipayNotifyClient.CertificateExecuteAsync<AlipayTradeAppPayNotify>(Request, _alipayOptionsAccessor.Value);
                if (notify.TradeStatus == AlipayTradeStatus.Success)//支付成功
                {

                    //await _ordersService.OrderPaySuccess(notify.OutTradeNo);
                    return AlipayNotifyResult.Success;
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("AliPayCallBack:", ex);
                return NoContent();
            }
        }


        /// <summary>
        /// 统一下单支付结果通知
        /// </summary>
        [HttpPost(nameof(WeChatPayCallBack))]
        public async Task<IActionResult> WeChatPayCallBack()
        {
            try
            {
                var notify = await _weChatPayNotifyClient.ExecuteAsync<WeChatPayUnifiedOrderNotify>(Request, _weChatPayOptionsAccessor.Value);
                if (notify.ReturnCode == WeChatPayCode.Success)
                {

                    if (notify.ResultCode == WeChatPayCode.Success)
                    {
                        //await _ordersService.OrderPaySuccess(notify.OutTradeNo);

                        return WeChatPayNotifyResult.Success;
                    }
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog("WeChatPayCallBack:", ex);
                return NoContent();
            }
        }

        #endregion



    }
}