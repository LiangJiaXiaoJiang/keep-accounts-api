﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KeepAccounts.Models;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Services.Domain;

namespace KeepAccounts.Api.Areas.App.Controllers
{
    [ApiExplorerSettings(GroupName = Grouping.V3)]
    [Route("App/[controller]")]
    [ApiController] 
    [Authorize]
    public class HomeController : ControllerBase
    {
        private IUserInfoServices _userInfoServices;

        public HomeController(IUserInfoServices userInfoServices) {

            _userInfoServices = userInfoServices;
        }

        [HttpPost(nameof(Index))]
        public ResponseModel Index()
        { 
            return ResponseModel.Error("App/Home/Index");
        }

    }
}