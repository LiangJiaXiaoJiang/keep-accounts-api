﻿using KeepAccounts.Models;
using KeepAccounts.Models.Models;
using KeepAccounts.Models.Models.Book;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Repository.Book;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeepAccounts.Api.Areas.App.Controllers
{
    /// <summary>
    /// 分类
    /// </summary>
    [ApiExplorerSettings(GroupName = Grouping.V3)]
    [Route("App/[controller]")]
    [ApiController]
    [Authorize]
    public class BookController : ControllerBase
    {
        private readonly IBookServices _bookServices;

        public BookController(IBookServices bookServices)
        {
            _bookServices = bookServices;
        }


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>


        [HttpPost(nameof(Save))]
        public async Task<ResponseModel> Save(BookSaveQueryModel model)
        {

            return _bookServices.Save(model) ? ResponseModel.Success() : ResponseModel.Error("保存失败");
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost(nameof(GetBookList))]
        public async Task<ResponseModel> GetBookList(BookListQueryModel query)
        {

            return ResponseModel.Success(await _bookServices.GetBookList(query));
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost(nameof(DeleteBook))]
        public async Task<ResponseModel> DeleteBook(GuidQueryModel query)
        {

            return ResponseModel.Success(await _bookServices.DeleteBook(query));
        }
    }
}
