using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using KeepAccounts.Common.HttpContextUser;
using KeepAccounts.Common.Jwt;
using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using KeepAccounts.Api.Extensions;
using KeepAccounts.Common.Extensions;
using KeepAccounts.Services.Domain;
using KeepAccounts.Service.Domain.Sms;
using Essensoft.AspNetCore.Payment.Alipay;
using Essensoft.AspNetCore.Payment.WeChatPay;
using Senparc.CO2NET;
using Senparc.Weixin.Entities;
using Microsoft.Extensions.Options;
using Senparc.CO2NET.RegisterServices;
using Senparc.Weixin;
using Senparc.Weixin.RegisterServices;
using Microsoft.Extensions.Hosting.Internal;

namespace KeepAccounts.Api
{
    public class Startup
    {
        public static ILoggerRepository repository { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            repository = LogManager.CreateRepository("NETCoreRepository");
            var fileinfo = new FileInfo("Config/log4net.config");
            XmlConfigurator.Configure(repository, fileinfo);
            InitRepository.LogRepository = repository;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();//将HttpClient注入进来

            #region 返回数据序列化设置

            //添加这个大小写设置才有用
            services.AddControllers()
            .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // 忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                // 不使用驼峰
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                // 设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                // 如字段为null值，该字段不会返回到前端
                // options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            #endregion

            #region 跨域
            //跨域 
            services.AddCors();
            #endregion

            #region 支付
            // 引入Payment 依赖注入
            services.AddAlipay();
            services.AddWeChatPay();
            services.Configure<AlipayOptions>(Configuration.GetSection("Alipay"));
            services.Configure<WeChatPayOptions>(Configuration.GetSection("WeChatPay"));
            #endregion

            #region Jwt
            services.Configure<JwtSettings>(Configuration.GetSection("JwtSettings"));
            var jwtSettings = new JwtSettings();
            Configuration.Bind("JwtSettings", jwtSettings);
            services.AddAuthentication(options =>
            {
                //  option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;

                //认证middleware配置
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(bearer =>
            {
                bearer.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(jwtSettings.SecretKey)),
                    ValidIssuer = jwtSettings.Issuer,//发行人
                    ValidAudience = jwtSettings.Audience,//订阅人
                    ClockSkew = TimeSpan.Zero
                    //  RequireExpirationTime = true,

                };
            });
            #endregion

            services.AddMemoryCache();//使用本地缓存必须添加

            #region HttpContextSetup
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAspNetUser, AspNetUser>();
            #endregion


            // services.AddScoped<ISmsService>(s => new  SmsService(s.GetRequiredService<KeepAccounts.Repository.Sms.ISmsRepository>() ));
           
            services.AddHealthChecks();
            //services.AddControllers();

            services.AddSwaggerSetup();

            services.AddSqlsugarSetup();

            services.AddSenparcGlobalServices(Configuration)//Senparc.CO2NET 全局注册
        .AddSenparcWeixinServices(Configuration);//Senparc.Weixin 注册 



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<SenparcSetting> senparcSetting, IOptions<SenparcWeixinSetting> senparcWeixinSetting)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            //SwaggerUI
            app.UseSwaggerUI();


            app.UseHealthChecks("/Health");


            // 启动 CO2NET 全局注册，必须！
            IRegisterService register = RegisterService.Start(senparcSetting.Value)
                                                        //关于 UseSenparcGlobal() 的更多用法见 CO2NET Demo：https://github.com/Senparc/Senparc.CO2NET/blob/master/Sample/Senparc.CO2NET.Sample.netcore/Startup.cs
                                                        .UseSenparcGlobal();

            register.UseSenparcWeixin(senparcWeixinSetting.Value, senparcSetting.Value);//微信全局注册，必须！


            //跨域
            app.UseCors(options => options.SetIsOriginAllowed(x => _ = true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());


            //将 对象 IHttpContextAccessor 注入 HttpContextHelper 静态对象中
            HttpContextHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseStaticFiles();

            app.UseMiddleware(typeof(GlobalExceptions));

            //强制跳转https
            //app.UseHttpsRedirection();

            app.UseRouting();

            //授权
            app.UseAuthentication();
            //认证
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });



            //var configurationRoot = AppConfigure.GetConfigurationRoot(env.ContentRootPath, env.EnvironmentName);

            //Console.WriteLine(Common.Config.WebConfig.WeCahtAppId);
            //Console.WriteLine(configurationRoot["WeChat:appId"]);
        }


        /// <summary>
        /// Autofac
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModuleRegister());
        }

    }
}
