﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters.Xml;
using Microsoft.Extensions.Logging;
using KeepAccounts.Models;
using KeepAccounts.Models.Swagger;
using KeepAccounts.Services.Domain;

namespace KeepAccounts.Api.Controllers
{


    [ApiExplorerSettings(GroupName = Grouping.V1)]
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<HomeController> _logger;
        private readonly IUserInfoServices _userInfoServices;

        public HomeController(ILogger<HomeController> logger, IUserInfoServices userInfoServices)
        {
            _logger = logger;
            _userInfoServices = userInfoServices;
        }

        [HttpPost(nameof(Index))]
        public async Task<ResponseModel> Index()
        {
             var model = await _userInfoServices.QueryById(new Guid());
            return ResponseModel.Error("");


        }
    }
}
