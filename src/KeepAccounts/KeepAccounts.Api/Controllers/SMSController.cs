﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KeepAccounts.Service.Domain.Sms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KeepAccounts.Models;
using KeepAccounts.Models.Models.Sms;
using KeepAccounts.Models.Swagger;

namespace KeepAccounts.Api.Controllers
{


    /// <summary>
    /// 手机验证码
    /// </summary>
    [ApiExplorerSettings(GroupName = Grouping.V1)]
    [Route("Common/[controller]")]
    [ApiController]
    public class SMSController : ControllerBase
    {

        ISmsService _smsService;
        public SMSController(ISmsService smsService)
        {
            _smsService = smsService;
        }


        /// <summary>
        /// 发送短信验证码
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(SendPhoneCodeSms))]
        public async Task<ResponseModel> SendPhoneCodeSms(SendPhoneCodeModel model)
        {
            return ResponseModel.Success(await _smsService.SendPhoneCode(model));

        }


    }
}