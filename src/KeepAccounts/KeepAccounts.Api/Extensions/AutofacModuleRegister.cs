﻿using Autofac;
using KeepAccounts.Repository;
using KeepAccounts.Services; 
using System.Collections.Generic;
using System.Reflection;

namespace KeepAccounts.Api.Extensions
{
    public class AutofacModuleRegister : Autofac.Module 
    {
        protected override void Load(ContainerBuilder builder)
        {

            base.Load(builder);
            var assemblies = new List<Assembly>
            {  
                typeof(RepositoryModule).Assembly,
                typeof(ServicesModule).Assembly
            };

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces().PropertiesAutowired();
            builder.RegisterAssemblyTypes(assemblies.ToArray()).AsImplementedInterfaces().PropertiesAutowired();
        }

    }
}
