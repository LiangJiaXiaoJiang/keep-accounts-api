﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using KeepAccounts.Models;
using KeepAccounts.Models.Base;
using KeepAccounts.Models.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KeepAccounts.Api.Extensions
{
    public static class SwaggerSetup
    {
        public static string ApiName { get; set; } = "**接口";


        /// <summary>
        /// Swagger描述信息
        /// </summary>
        private static readonly string description = "Apidescription";


        private static readonly List<SwaggerApiInfo> swaggerApiInfos = new List<SwaggerApiInfo>  {
             
         new SwaggerApiInfo{ Name=Grouping.V3,UrlPrefix=Grouping.V3 ,OpenApiInfo=new OpenApiInfo{ Version="v1",Title=Grouping.V3,Description=description } },


        new SwaggerApiInfo{ Name=Grouping.V1,UrlPrefix=Grouping.V1 ,OpenApiInfo=new OpenApiInfo{ Version="v1",Title=Grouping.V1,Description=description } },


        new SwaggerApiInfo{ Name=Grouping.V2,UrlPrefix=Grouping.V2 ,OpenApiInfo=new OpenApiInfo{ Version="v1",Title=Grouping.V2,Description=description } },


          
        };


        public static void AddSwaggerSetup(this IServiceCollection services)
        {
            services.AddSwaggerGen(option =>
            {
                  
                // 遍历并应用Swagger分组信息
                swaggerApiInfos.ForEach(x =>
                {
                    option.SwaggerDoc(x.UrlPrefix, x.OpenApiInfo);
                });

                // include document file

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                option.IncludeXmlComments(Path.Combine(basePath, $"{typeof(Startup).Assembly.GetName().Name}.xml"), true);
                option.IncludeXmlComments(Path.Combine(basePath, $"{typeof(ResponseModel).Assembly.GetName().Name}.xml"),true);


                #region JwT Bearer
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "请输入Token",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                });
                option.AddSecurityRequirement(new OpenApiSecurityRequirement {
                { new OpenApiSecurityScheme   {
                    Reference = new OpenApiReference()  {
                Id = "Bearer",
                Type = ReferenceType.SecurityScheme
                     }
              }, Array.Empty<string>() }
              });
                #endregion


            });
        }

        /// <summary>
        /// 自定义SwaggerUI扩展
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerUI(this IApplicationBuilder app)
        {

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                // 遍历分组信息，生成Json
                swaggerApiInfos.ForEach(x =>
                {
                    options.SwaggerEndpoint($"/swagger/{x.UrlPrefix}/swagger.json", x.Name);
                });
                // 模型的默认扩展深度，设置为 -1 完全隐藏模型
                //options.DefaultModelsExpandDepth(-1);
                // API文档仅展开标记
                options.DocExpansion(DocExpansion.List);
                // API前缀设置为空
                options.RoutePrefix = "swaggerui";
                // API页面Title
                options.DocumentTitle = ApiName;



            });
        }

    }


    public class SwaggerApiInfo
    {
        /// <summary>
        /// URL前缀
        /// </summary>
        public string UrlPrefix { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// <see cref="Microsoft.OpenApi.Models.OpenApiInfo"/>
        /// </summary>
        public OpenApiInfo OpenApiInfo { get; set; }

    }
}
