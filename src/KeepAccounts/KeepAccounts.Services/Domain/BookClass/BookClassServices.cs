﻿using KeepAccounts.Common.Config;
using KeepAccounts.Common.HttpContextUser;
using KeepAccounts.Models;
using KeepAccounts.Models.Models; 
using KeepAccounts.Models.Models.BookClass;
using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Models.PageModel;
using KeepAccounts.Models.ViewModels.BookClass; 
using KeepAccounts.Services.BaseServices;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KeepAccounts.Repository.BookClass
{
    public class BookClassServices : BaseServices<BookClassEntity>, IBookClassServices
    {
        private readonly IBookClassRepository _bookClassRepository;
        private readonly IAspNetUser _aspNetUser;

        public BookClassServices(IBookClassRepository dal, IAspNetUser aspNetUser)
        {
            base.BaseDal = dal;
            _bookClassRepository = dal;
            _aspNetUser = aspNetUser; 
        }

        public Task<bool> Delete(GuidQueryModel query)
        {
            return _bookClassRepository.DeleteById(query.Id);
        }

        public Task<PageResultModel<BookClassEntity>> FindList(BookClassListQueryModel query)
        {
            query.WhereLambda = Expressionable.Create<BookClassEntity>().
                  //And(s => !s.IsDeleted).
                  AndIF(!string.IsNullOrWhiteSpace(query.Name), s => s.Name.Contains(query.Name)).

                  ToExpression();

            return _bookClassRepository.Pages<BookClassEntity>(query);
        }

        public Task<PageResultModel<BookClassListViewModel>> GetBookClassList(BookClassListQueryModel query)
        {
            query.WhereLambda = Expressionable.Create<BookClassEntity>().
                 //And(s => !s.IsDeleted).
                 AndIF(!string.IsNullOrWhiteSpace(query.Name), s => s.Name.Contains(query.Name)).

                 ToExpression();

            return _bookClassRepository.Pages<BookClassListViewModel>(query);
        }

        /// <summary>
        /// 保存修改信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Save(BookClassEntity model)
        {
            CheckModel(model);



            if (model.ID == Guid.Empty)
            { //保存
                model.ID = Guid.NewGuid();
                model.UserId = _aspNetUser.UserId;

                model.CreateUserID = _aspNetUser.UserId;
                return _bookClassRepository.Add(model).Result > 0;
            }
            else
            { //修改 


                model.UpdateUserID = _aspNetUser.UserId;
                model.UpdateTime = DateTime.Now;
                return _bookClassRepository.Update(model).Result;
            }
        }
          
        private void CheckModel(BookClassEntity model)
        {
            if (model == null)
                throw new ApiException("保存信息有误");
            if (string.IsNullOrWhiteSpace(model.Name))
                throw new ApiException("名称不能为空");



        }

    }
}
