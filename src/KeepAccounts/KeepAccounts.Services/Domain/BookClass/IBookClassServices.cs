﻿using KeepAccounts.Models.Models;
using KeepAccounts.Models.Models.BookClass;
using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Models.PageModel;
using KeepAccounts.Models.ViewModels.BookClass;
using KeepAccounts.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KeepAccounts.Repository.BookClass
{
    public interface IBookClassServices : IBaseServices<BookClassEntity>
    {
        Task<PageResultModel<BookClassEntity>> FindList(BookClassListQueryModel query);
        bool Save(BookClassEntity model);

        Task<bool> Delete(GuidQueryModel model);

        Task<PageResultModel<BookClassListViewModel>> GetBookClassList(BookClassListQueryModel query);
    }
}
