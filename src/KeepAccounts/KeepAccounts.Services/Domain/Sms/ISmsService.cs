﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KeepAccounts.Models.Models.Sms;

namespace KeepAccounts.Service.Domain.Sms
{
    public interface ISmsService
    {
        Task<object> SendPhoneCode(SendPhoneCodeModel model);
    }
}
