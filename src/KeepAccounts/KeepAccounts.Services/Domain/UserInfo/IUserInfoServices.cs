﻿using KeepAccounts.Models.Models.UserInfo;
using KeepAccounts.Models.PageModel;
using KeepAccounts.Models.ViewModels.UserInfo;
using KeepAccounts.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using KeepAccounts.Models.Models;

namespace KeepAccounts.Services.Domain
{
    public interface IUserInfoServices : IBaseServices<UserInfoEntity>
    {
        Task<LoginResultViewModel> Login(LoginQueryModel model);

        LoginResultViewModel OAuthLogin(OAuthLoginQueryModel model);

        Task<UserInfoResultViewModel> GetUserInfo();

        Task<bool> Logout();

        Task<bool> UpdatePassword(UpdatePasswordQueryModel query);



        Task<bool> FindPassword(FindPasswordQueryModel model);

        Task<bool> ResetPassword(ResetPasswordQueryModel model);

        Task<PageResultModel<UserInfoEntity>> FindList(UserInfoListQueryModel query);



        Task<bool> UpdateUserInfo(UpdateUserInfoQueryModel query);


        bool Save(UserInfoEntity model);

        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> Register(RegisterQueryModel model);


        Task<LoginResultViewModel> RefreshToken(string token);
    }
}
