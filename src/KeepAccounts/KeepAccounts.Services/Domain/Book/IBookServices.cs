﻿using KeepAccounts.Models.Models;
using KeepAccounts.Models.Models.Book;
using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Models.PageModel;
using KeepAccounts.Models.ViewModels.Book;
using KeepAccounts.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KeepAccounts.Repository.Book
{
    public interface IBookServices : IBaseServices<BookEntity>
    {
        Task<PageResultModel<BookEntity>> FindList(BookListQueryModel query);

        bool Save(BookEntity model);

        bool Save(BookSaveQueryModel model);

        Task<bool> DeleteBook(GuidQueryModel query);
        Task<bool> Delete(GuidQueryModel model);

        Task<IndexBookListViewModel> GetBookList(BookListQueryModel query);
    }
}
