﻿using KeepAccounts.Models.Enums.Sms;
using KeepAccounts.Models.Models.Sms;
using KeepAccounts.Repository.BaseRepository;
using System.Threading.Tasks;

namespace KeepAccounts.Repository.Sms
{
    public interface ISmsRepository : IBaseRepository<SmsEntity>
    {
        Task<SmsEntity> GetPhoneCode(string phoneNumber, SmsTypeEnum smsType = SmsTypeEnum.验证码);
    }
}