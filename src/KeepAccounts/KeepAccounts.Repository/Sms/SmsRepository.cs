﻿
using KeepAccounts.Models.Enums.Sms;
using KeepAccounts.Models.Models.Sms;
using KeepAccounts.Repository.BaseRepository;
using KeepAccounts.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepAccounts.Repository.Sms
{
    class SmsRepository : BaseRepository<SmsEntity>, ISmsRepository
    {
        public SmsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        #region 获得发送的短信验证码信息
        /// <summary>
        /// 获得发送的短信验证码信息
        /// </summary>
        /// <returns></returns>
        public async Task<SmsEntity> GetPhoneCode(string phoneNumber, SmsTypeEnum smsType = SmsTypeEnum.验证码)
        {
            //TODO t.IsSuccess &&先不验证是否发送成功
            var smsList = await Query(t =>
                                           t.PhoneNumbers == phoneNumber &&
                                           t.Type == smsType.GetHashCode() &&
                                           t.UpdateTime > DateTime.Now.AddMinutes(-5),
                t => t.UpdateTime,
                false);
            return smsList.FirstOrDefault();
        }
        #endregion
    }
}

