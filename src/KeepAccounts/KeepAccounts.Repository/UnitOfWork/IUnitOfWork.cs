﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.UnitOfWork
{
    public interface IUnitOfWork
    {
        ISqlSugarClient GetDbClient();

        void BeginTran();

        void CommitTran();
        void RollbackTran();
    }
}
