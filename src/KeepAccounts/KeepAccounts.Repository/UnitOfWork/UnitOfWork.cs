﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KeepAccounts.Repository.UnitOfWork
{

    public class UnitOfWork : IUnitOfWork
    {

        private readonly ISqlSugarClient _sqlSugarClient;

        public UnitOfWork(ISqlSugarClient sqlSugarClient)
        {
            //sqlSugarClient.CodeFirst.InitTables(typeof(Models.UserInfo));
            //sqlSugarClient.CodeFirst.InitTables(typeof(Models.ReportModel));

            #region 通过反射注册表
            //var assembly = Assembly.Load("KeepAccounts.Models");

            //foreach (var item in assembly.GetTypes())
            //{
            //    if (item.BaseType == typeof(Models.Base.BaseEntity))
            //    {
            //        sqlSugarClient.CodeFirst.InitTables(item);
            //    }
            //}
            #endregion
            //sqlSugarClient.CodeFirst.InitTables(typeof(ReportCommentLikeModel));
            //sqlSugarClient.CodeFirst.InitTables(typeof(Models.Models.CompanyInfo.ActivityEntity));
            //sqlSugarClient.CodeFirst.InitTables(typeof(Models.Models.CompanyInfo.LivePlayEntity));
            _sqlSugarClient = sqlSugarClient;
        }

        public ISqlSugarClient GetDbClient()
        {

            return _sqlSugarClient;
        }

        public void BeginTran()
        {
            GetDbClient().Ado.BeginTran();
        }

        public void CommitTran()
        {
            try
            {
                GetDbClient().Ado.CommitTran(); //
            }
            catch (Exception ex)
            {
                GetDbClient().Ado.RollbackTran();
                throw ex;
            }
        }

        public void RollbackTran()
        {
            GetDbClient().Ado.RollbackTran();
        }

    }
}
