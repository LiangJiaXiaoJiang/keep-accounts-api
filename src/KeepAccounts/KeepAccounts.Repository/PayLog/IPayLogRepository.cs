﻿using KeepAccounts.Models.Models.PayLog;
using KeepAccounts.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.PayLog
{
    public interface IPayLogRepository:IBaseRepository<PayLogEntity>
    {
    }
}
