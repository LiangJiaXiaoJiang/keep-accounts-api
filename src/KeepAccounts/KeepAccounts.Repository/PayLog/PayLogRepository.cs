﻿using KeepAccounts.Models.Models.PayLog;
using KeepAccounts.Repository.BaseRepository;
using KeepAccounts.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.PayLog
{
    public class PayLogRepository : BaseRepository<PayLogEntity>, IPayLogRepository
    {
        public PayLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

    }
}
