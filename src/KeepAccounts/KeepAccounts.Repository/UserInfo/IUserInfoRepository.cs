﻿using KeepAccounts.Models.Models.UserInfo;
using KeepAccounts.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KeepAccounts.Repository
{
    public interface IUserInfoRepository:IBaseRepository<UserInfoEntity>
    {
        
    }
}
