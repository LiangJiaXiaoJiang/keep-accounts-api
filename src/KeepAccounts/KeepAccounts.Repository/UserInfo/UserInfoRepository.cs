
﻿using Newtonsoft.Json;
using KeepAccounts.Models.Models.UserInfo;
using KeepAccounts.Models.PageModel;
using KeepAccounts.Repository.BaseRepository;
using KeepAccounts.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks; 

namespace KeepAccounts.Repository
{
    public class UserInfoRepository : BaseRepository<UserInfoEntity>, IUserInfoRepository
    {
        public UserInfoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
       
        
    }
}
