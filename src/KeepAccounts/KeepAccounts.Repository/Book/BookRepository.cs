﻿using KeepAccounts.Models.Models.Book; 
using KeepAccounts.Repository.BaseRepository;
using KeepAccounts.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.Book
{
    public class BookRepository:BaseRepository<BookEntity>,IBookRepository
    {
        public BookRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        { 
        }


    }
}
