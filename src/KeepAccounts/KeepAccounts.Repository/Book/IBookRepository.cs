﻿using KeepAccounts.Models.Models.Book; 
using KeepAccounts.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.Book
{
    public interface IBookRepository : IBaseRepository<BookEntity>
    {
    }
}
