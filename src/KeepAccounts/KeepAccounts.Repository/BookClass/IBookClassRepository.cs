﻿using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.BookClass
{
    public interface IBookClassRepository : IBaseRepository<BookClassEntity>
    {
    }
}
