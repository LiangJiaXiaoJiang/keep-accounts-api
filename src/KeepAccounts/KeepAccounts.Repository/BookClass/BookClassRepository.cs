﻿using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Repository.BaseRepository;
using KeepAccounts.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Repository.BookClass
{
    public class BookClassRepository:BaseRepository<BookClassEntity>,IBookClassRepository
    {
        public BookClassRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        { 
        }


    }
}
