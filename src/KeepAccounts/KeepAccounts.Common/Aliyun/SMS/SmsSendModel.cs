﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Aliyun.SMS
{
    public class SmsSendModel
    {
        /// <summary>
        /// 手机号列表
        /// </summary>
        public List<string> PhoneNumbers { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string SignName { get; set; }
        /// <summary>
        /// 模板编号
        /// </summary>
        public string TemplateCode { get; set; }
        /// <summary>
        /// 模板参数
        /// </summary>
        public string TemplateParam { get; set; }
        /// <summary>
        /// 上行短信扩展码 暂时无用
        /// </summary>
        public string SmsUpExtendCode { get; set; }
        /// <summary>
        /// 短信流水号  外部流水扩展字段。
        /// </summary>
        public string OutId { get; set; }
    }
}
