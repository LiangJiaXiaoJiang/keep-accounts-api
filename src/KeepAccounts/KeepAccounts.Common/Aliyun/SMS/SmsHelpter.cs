﻿using System;
using System.Collections.Generic;
using System.Text;
using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using KeepAccounts.Common.Config; 
using KeepAccounts.Common.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KeepAccounts.Common.Aliyun.SMS
{
    public class SmsHelpter
    {
        #region SendSms
        /// <summary>
        /// 发送短信
        /// </summary>
        /// <returns></returns>
        public static SmsSendResultModel SendSms(SmsSendModel model)
        {
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", WebConfig.SmsAccessKeyId, WebConfig.SmsAccessSecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.Method = MethodType.POST;
            request.Domain = "dysmsapi.aliyuncs.com";
            request.Version = "2017-05-25";
            request.Action = "SendSms";
            request.AddQueryParameters("PhoneNumbers", string.Join(",", model.PhoneNumbers));
            request.AddQueryParameters("SignName", model.SignName);
            request.AddQueryParameters("TemplateCode", model.TemplateCode);
            request.AddQueryParameters("TemplateParam", model.TemplateParam);
            if (!string.IsNullOrEmpty(model.OutId))
            {
                request.AddQueryParameters("OutId", model.OutId);
            }
            try
            {
                CommonResponse response = client.GetCommonResponse(request);
                var result = Encoding.Default.GetString(response.HttpResponse.Content);
                return JsonConvert.DeserializeObject<SmsSendResultModel>(result);
            }
            catch (ServerException e)
            {
                //LogHelper.WriteLog("SendSms:ServerException", e);
            }
            catch (ClientException e)
            {
                //LogHelper.WriteLog("SendSms:ClientException", e);
            }

            return new SmsSendResultModel();
        }
        #endregion
    }
}
