﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Aliyun.SMS
{
    public class SmsSendResultModel
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 消息码
        /// </summary>
        public string Code { get; set; }

        public string RequestId { get; set; }
        /// <summary>
        /// 流水号
        /// </summary>
        public string BizId { get; set; }
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess
        {
            get { return Code.ToLower().Equals("ok"); }
        }
    }
}
