﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Jwt
{
    public class JwtSettings
    {
        /// <summary>
        /// 私有变量
        /// </summary>
        private static JwtSettings _settings = null;


        public JwtSettings()
        {

        }

        public JwtSettings(IOptions<JwtSettings> jwtSettings)
        {
            _settings = jwtSettings.Value;
        }

        ///// <summary>
        ///// 暴露的实体
        ///// </summary>
        //public static JwtSettings Settings { get { if (_settings == null) _settings = new JwtSettings(); return _settings; } }


        /// <summary>
        /// token是谁颁发的
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// token可以给哪些客户端使用
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 加密的key
        /// </summary>
        public string SecretKey { get; set; }
    }

    //public class AppSettingConfig
    //{

    //    public static JwtSettings jwtSetting;
    //    public AppSettingConfig(IOptions<JwtSettings> jwtSettings)
    //    {
    //        jwtSetting = jwtSettings.Value;
    //    }

    //    public static JwtSettings GetJwtSettings()
    //    {

    //        return jwtSetting;

    //    }

    //}

}

