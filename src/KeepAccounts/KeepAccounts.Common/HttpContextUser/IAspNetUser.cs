﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace KeepAccounts.Common.HttpContextUser
{
    public interface IAspNetUser
    {
        Guid UserId { get; }

        string ClientIP { get; }

        int ClientIdentityType { get; }


        Guid? CompanyId { get; }

        bool IsAuthenticated();
        IEnumerable<Claim> GetClaimsIdentity();
        List<string> GetClaimValueByType(string ClaimType);

        string GetToken();
        List<string> GetUserInfoFromToken(string ClaimType);
    }
}