﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Extensions
{
    using Microsoft.AspNetCore.Http;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// HttpContext注入类
    /// </summary>
    public static class HttpContextHelper
    {

        private static IHttpContextAccessor _accessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _accessor = httpContextAccessor;
        }

        public static HttpContext HttpContext => _accessor.HttpContext;


        public static string GetRequestUrlPath()
        {
            return $"{_accessor.HttpContext.Request.Scheme}://{_accessor.HttpContext.Request.Host}";//{source.QueryString}
        }

    }
}
