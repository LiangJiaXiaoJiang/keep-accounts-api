﻿using log4net;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Extensions
{


    public class InitRepository
    {
        public static ILoggerRepository LogRepository { get; set; }
    }

    public class LogHelper
    {

        //全局异常错误记录
        private static readonly ILog logError = LogManager.GetLogger(InitRepository.LogRepository.Name,"logError");

        //全局异常错误记录
        private static readonly ILog logInfo = LogManager.GetLogger(InitRepository.LogRepository.Name, "logInfo");


        #region 全局异常错误记录持久化
        /// <summary>
        /// 全局异常错误记录持久化
        /// </summary>
        /// <param name="throwMsg"></param>
        /// <param name="ex"></param>
        public static void ErrorLog(string throwMsg, Exception ex)
        {
            string errorMsg = string.Format("【抛出信息】：{0} <br>【异常类型】：{1} <br>【异常信息】：{2} <br>【堆栈调用】：{3}", new object[] { throwMsg,
                ex.GetType().Name, ex.Message, ex.StackTrace });
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            errorMsg = errorMsg.Replace("位置", "<strong style=\"color:red\">位置</strong>");
            logError.Error(errorMsg);
        }
        #endregion


        #region 自定义操作记录
        /// <summary>
        /// 自定义操作记录，与仓储中的增删改的日志是记录同一张表
        /// </summary>
        /// <param name="throwMsg"></param>
        /// <param name="ex"></param>
        public static void WriteLog(string throwMsg, Exception ex)
        {
            string errorMsg = string.Format("【抛出信息】：{0} <br>【异常类型】：{1} <br>【异常信息】：{2} <br>【堆栈调用】：{3}", new object[] { throwMsg,
                ex.GetType().Name, ex.Message, ex.StackTrace });
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            errorMsg = errorMsg.Replace("位置", "<strong style=\"color:red\">位置</strong>");
            logInfo.Info(errorMsg);
        }
        #endregion
    }
}
