﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Utilities
{


    public static class CacheHelper
    {

        public static MemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions() { });

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Set(string key, string value)
        {
            _memoryCache.Set(key, value);
        }


        public static string Get(string key)
        {

            var value = _memoryCache.Get(key);
            return value == null ? "" : value.ToString();
        }

    }
}
