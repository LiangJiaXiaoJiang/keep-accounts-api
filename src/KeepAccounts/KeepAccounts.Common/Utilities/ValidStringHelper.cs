﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Utilities
{
    public static class ValidStringHelper
    {

        /// <summary>
        /// 验证手机号
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool ValidPhoneNumber(string str)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(str, @"^[1]+[3,4,5,6,7,8,9]+\d{9}");
        }

    }
}
