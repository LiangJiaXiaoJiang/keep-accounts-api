﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.Utilities
{
    public class Com
    {
        /// <summary>
        /// author:gl func:转换工作经验
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetWorkYear(string time)
        {
            DateTime now = DateTime.Now;
            DateTime t = DateTime.Now;
            if (!DateTime.TryParse(time, out t))
            {
                return "1年";
            }

            int diffDay = (now - t).Days;
            double year = Math.Ceiling(diffDay / 365.0 - 1);
            return $"{year}年";
        }
        /// <summary>
        /// author:gl func:转换年龄
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetAgeYear(string time)
        {
            DateTime now = DateTime.Now;
            DateTime t = DateTime.Now;
            if (!DateTime.TryParse(time, out t))
            {
                return "1岁";
            }

            int diffDay = (now - t).Days;
            double year = Math.Ceiling(diffDay / 365.0 - 1);
            return $"{year}岁";
        }
        /// <summary>
        /// author:gl func:枚举 int 转 枚举名称
        /// </summary>
        /// <typeparam name="T">枚举</typeparam>
        /// <param name="itemValue">int值</param>
        /// <returns></returns>
        public static string ConvertEnumToString<T>(int itemValue)
        {
            return Enum.Parse(typeof(T), itemValue.ToString()).ToString().Replace("_", "-");//下划线改为横线
        }
    }
}
