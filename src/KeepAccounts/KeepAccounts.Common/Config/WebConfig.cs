﻿using Aliyun.Acs.Core.Retry;
using Microsoft.AspNetCore.Http.Extensions;
using KeepAccounts.Common.Extensions;
using KeepAccounts.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace KeepAccounts.Common.Config
{
    public static class WebConfig
    {
        #region AliyunSms
        /// <summary>
        /// 短信AccessKey
        /// </summary>
        public static string SmsAccessKeyId => ManagerConfig.GetManagerConfig("aliyun:Sms:AccessKey");
        /// <summary>
        /// 短信SmsAccessSecret
        /// </summary>
        public static string SmsAccessSecret => ManagerConfig.GetManagerConfig("aliyun:Sms:AccessSecret");
        /// <summary>
        /// 短信验证码模板编号
        /// </summary>
        public static string SmsSendPhoneCode => ManagerConfig.GetManagerConfig("aliyun:Sms:templates:phoneCode");
        /// <summary>
        /// 短信签名
        /// </summary>
        public static string SmsSignName => ManagerConfig.GetManagerConfig("aliyun:Sms:SignName");
        #endregion


        #region Pay
        /// <summary>
        /// 回调Url
        /// </summary>
        public static string AliPayNotifyUrl = "http://api.KeepAccounts.lostsea.cn/api/pay/aliPayCallBack";
        /// <summary>
        /// 充值回调Url
        /// </summary>
        public static string AliPayRechargeNotifyUrl = "http://api.KeepAccounts.lostsea.cn/api/pay/aliPayRechargeCallBack";
        /// <summary>
        /// 支付产品编号
        /// </summary>
        public static string AliPayProductCode = "QUICK_MSECURITY_PAY";
        /// <summary>
        /// 微信回调地址
        /// </summary>
        public static string WxPayNotifyUrl = "http://api.KeepAccounts.lostsea.cn/api/pay/WeChatPayCallBack";
        /// <summary>
        /// 退款回调地址
        /// </summary>
        public static string WxPayRefundNotifyUrl = "http://api.KeepAccounts.lostsea.cn/api/pay/WeChatRefundCallBack";

        /// <summary>
        /// 微信充值回调地址
        /// </summary>
        public static string WxPayRechargeNotifyUrl = "http://api.KeepAccounts.lostsea.cn/api/pay/WeChatRechargeCallBack";

        #endregion

        /// <summary>
        /// 当前网址
        /// </summary>
        public static string WebSiteUrl =string.IsNullOrWhiteSpace( ManagerConfig.GetManagerConfig("WebSiteConfig:WebUrl"))? HttpContextHelper.GetRequestUrlPath(): ManagerConfig.GetManagerConfig("WebSiteConfig:WebUrl"); //HttpContextHelper.GetRequestUrlPath();



        //这是测试appId
        public static string WeCahtAppId = ManagerConfig.GetManagerConfig("WeChat:appId");
        public static string WeCahtAppSecert = ManagerConfig.GetManagerConfig("WeChat:appSecret");
        //string Url = ManagerConfig.GetManagerConfig("Url:webRL");


        public static string ResumeParseUrl = ManagerConfig.GetManagerConfig("ResumeParseUrl");

    }
}
