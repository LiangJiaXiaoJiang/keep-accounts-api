﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.WeChat
{
    public class NativePay
    {

        /**
      * 生成直接支付url，支付url有效期为2小时,模式二
      * @param productId 商品ID
      * @return 模式二URL
      */
        public string GetPayUrl(string productId)
        {
            //LogHelper._Logger.Info(this.GetType().ToString(), "Native pay mode 2 url is producing...");

            WxPayData data = new WxPayData();
            data.SetValue("body", "test");//商品描述
            data.SetValue("attach", "test");//附加数据
            data.SetValue("out_trade_no", WxPayApi.GenerateOutTradeNo());//随机字符串
            data.SetValue("total_fee", 1);//总金额
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "jjj");//商品标记
            data.SetValue("trade_type", "NATIVE");//交易类型
            data.SetValue("product_id", productId);//商品ID

            WxPayData result = WxPayApi.UnifiedOrder(data);//调用统一下单接口
            string url = result.GetValue("code_url").ToString();//获得统一下单接口返回的二维码链接

            //LogHelper._Logger.Info(this.GetType().ToString(), "Get native pay mode 2 url : " + url);
            return url;
        }

        /// <summary>
        /// 酬帮支付统一下单
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public string GetPay(string out_trade_no, string productId,string body,string attach,int total_fee)
        {

            //LogHelper._Logger.Info(this.GetType().ToString(), "酬帮统一下单提交ing...");

            WxPayData data = new WxPayData();
            data.SetValue("body", body);//商品描述
            data.SetValue("attach", attach);//附加数据
            data.SetValue("out_trade_no", out_trade_no);//随机字符串
            data.SetValue("total_fee", total_fee);//总金额
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(30).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "SLY");//商品标记
            data.SetValue("trade_type", "APP");//交易类型
            data.SetValue("product_id", productId);//商品ID

            WxPayData result = WxPayApi.UnifiedOrder(data);//调用统一下单接口
                                                           // string url = result.GetValue("code_url").ToString();//获得统一下单接口返回的二维码链接

            //LogHelper._Logger.Info(this.GetType().ToString(), "Get native pay mode 2 url : " + url);
            return result.ToJson(); //Newtonsoft.Json.JsonConvert.SerializeObject(result);//result.GetValue("nprepay_id").ToString();

        }


    }
}
