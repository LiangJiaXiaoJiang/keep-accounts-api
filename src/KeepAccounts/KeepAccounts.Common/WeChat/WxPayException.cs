﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.WeChat
{
    public class WxPayException : Exception
    {
        public WxPayException(string msg) : base(msg)
        {

        }
    }
}
