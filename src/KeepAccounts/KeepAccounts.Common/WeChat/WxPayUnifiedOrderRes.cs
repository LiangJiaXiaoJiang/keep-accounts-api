﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Common.WeChat
{
    /// <summary>
    /// 统一下单返回数据
    /// </summary>
    public class WxPayUnifiedOrderRes
    {
        public string appid { get; set; }
        public string mch_id { get; set; }
        public string nonce_str { get; set; }
        public string prepay_id { get; set; }
        public string result_code { get; set; }
        public string return_code { get; set; }
        public string return_msg { get; set; }
        public string sign { get; set; }
        public string trade_type { get; set; }
    }



    /// <summary>
    /// 微信app支付需要的参数
    /// </summary>
    public class WxAppPayData
    {
        public string appid { get; set; }
        public string noncestr { get; set; }
        public string package { get; set; }
        public string partnerid { get; set; }
        public string prepayid { get; set; }
        public string sign { get; set; }
        public string timestamp { get; set; }
    }


}
