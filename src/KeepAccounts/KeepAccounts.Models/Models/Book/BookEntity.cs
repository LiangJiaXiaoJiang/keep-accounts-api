﻿using KeepAccounts.Models.Enums.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Book
{
    /// <summary>
    /// 账本
    /// </summary>
    [SqlSugar.SugarTable("Book")]
    public class BookEntity : Base.UserBaseEntity
    {

        /// <summary>
        /// 账单类型
        /// </summary>
        public BillTypeEnum BillType { get; set; }


        /// <summary>
        /// 金额
        /// </summary>
        public decimal AmountOfMoney { get; set; }

        /// <summary>
        /// 分类id
        /// </summary>
        public Guid ClassId { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime ConsumptionTime { get; set; }


        /// <summary>
        /// 支付方式 --预留 暂不实现功能
        /// </summary>
        public string PayType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 分类名
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 分类icon
        /// </summary>
        public string ClassIcon { get; set; }

        /// <summary>
        /// 分类bg
        /// </summary>
        public string ClassBg { get; set; }

    }
}
