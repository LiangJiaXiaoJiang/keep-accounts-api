﻿using KeepAccounts.Models.Enums.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Book
{
    /// <summary>
    /// 保存数据请求参数
    /// </summary>
    public class BookSaveQueryModel
    {
        /// <summary>
        /// 分类id
        /// </summary>
        public Guid ClassId { get; set; }
         
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime ConsumptionTime { get; set; }

        /// <summary>
        /// 账单类型
        /// </summary>
        public BillTypeEnum BillType { get; set; }


        /// <summary>
        /// 金额
        /// </summary>
        public decimal AmountOfMoney { get; set; }



        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


    }
}
