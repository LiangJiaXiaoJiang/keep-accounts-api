﻿using KeepAccounts.Models.PageModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Book
{/// <summary>
/// 列表查询
/// </summary>
    public class BookListQueryModel
    {
        /// <summary>
        /// 年月 yyyy-MM
        /// </summary>
        public string YearMonth { get; set; }

        /// <summary>
        /// 搜索
        /// </summary>
        public string Name { get; set; }

    }
}
