﻿using KeepAccounts.Models.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Sms
{
    /// <summary>
    /// 发送短信记录信息
    /// </summary>
    [SugarTable("Sms")]
    public class SmsEntity : BaseEntity
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumbers { get; set; }
        /// <summary>
        /// 发送类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 消息模板Id
        /// </summary>
        public string TemplateCode { get; set; }
        /// <summary>
        /// 模板参数
        /// </summary>
        public string TemplateParameter { get; set; }
        /// <summary>
        /// 返回参数
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable =true)]
        public string ResponseParameter { get; set; }
        /// <summary>
        /// 回执Id
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public string BizId { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
    }
}
