﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Sms
{

    /// <summary>
    /// 创建手机验证码
    /// </summary>
    public class SendPhoneCodeModel
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
