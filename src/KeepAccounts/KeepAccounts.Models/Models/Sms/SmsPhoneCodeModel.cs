﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.Sms
{
    /// <summary>
    /// 发送验证码
    /// </summary>
    public class SmsPhoneCodeModel
    {
        /// <summary>
        /// 验证码
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 产品信息
        /// </summary>
        public string product { get; set; }
    }
}
