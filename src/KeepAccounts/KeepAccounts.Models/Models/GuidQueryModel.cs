﻿using System;
using System.Collections.Generic;
using System.Text; 
namespace KeepAccounts.Models.Models
{

    
    /// <summary>
    /// GUID
    /// </summary>
    public class GuidQueryModel
    {

        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get; set; }

    }
}
