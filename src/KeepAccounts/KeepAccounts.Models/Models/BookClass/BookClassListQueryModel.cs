﻿using KeepAccounts.Models.Models.PageModel;
using KeepAccounts.Models.PageModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.BookClass
{
    public class BookClassListQueryModel:BasePageQuery<BookClassEntity>
    { 


        public string Name { get; set; }
    }
}
