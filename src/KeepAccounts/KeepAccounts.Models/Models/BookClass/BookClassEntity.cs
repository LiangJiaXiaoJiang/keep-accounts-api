﻿using KeepAccounts.Models.Enums.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.PageModel
{
    /// <summary>
    /// 账单类目 
    /// </summary>
    [SqlSugar.SugarTable("BookClass")]
    public class BookClassEntity:Base.BaseEntity
    {/// <summary>
     /// 分类名称
     /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 图标名称
        /// </summary>
        public string IconName { get; set; }


        /// <summary>
        /// 背景颜色
        /// </summary>
        public string BgColor { get; set; }

        /// <summary>
        /// 账单类型
        /// </summary>
        public BillTypeEnum BillType { get; set; }

        /// <summary>
        /// 用户id (每个用户单独的分类)
        /// </summary>
        public Guid UserId { get; set; }
    }
}
