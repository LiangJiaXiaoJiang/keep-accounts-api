﻿using KeepAccounts.Models.Enums.UserInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    public class UpdatePasswordQueryModel
    {


        /// <summary>
        /// 旧密码
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }


        /// <summary>
        /// 修改密码类型
        /// </summary>
        public  UpdatePasswordTypeEnum UpdatePasswordType { get; set; }
    }
}
