﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 注册
    /// </summary>
    public class RegisterQueryModel
    {

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 手机验证码
        /// </summary>
        public string PhoneCode { get; set; }

        /// <summary>
        /// 上级id
        /// </summary>
        public Guid ParentId { get; set; }



    }
}
