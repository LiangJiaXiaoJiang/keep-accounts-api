﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text;
using KeepAccounts.Models.Enums.UserInfo;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 创建用户
    /// </summary>
    public class CreateUserModel
    {
        /// <summary>
        /// 用户openid
        /// </summary>
        public string WeChatOpenId { get; set; }

        /// <summary>
        /// WeChatUnionId
        /// </summary>
        public string WeChatUnionId { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 上级id
        /// </summary>
        public Guid ParentId { get; set; } 

        /// <summary>
        /// 平台用户信息
        /// </summary>
        public WhatUserInfo UserInfo { get; set; }

    }
}
