﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// wx用户信息
    /// </summary>
    public class WhatUserInfo
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string nickName { get; set; }
        /// <summary>
        ///0	未知	
        ///1	男性	
        ///2	女性
        /// </summary>
        public int gender { get; set; }
        /// <summary>
        /// 语言
        /// </summary>
        public string language { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// 用户所在省份
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// 国家
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string avatarUrl { get; set; }

    }
}
