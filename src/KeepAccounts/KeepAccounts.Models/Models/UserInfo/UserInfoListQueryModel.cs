﻿using KeepAccounts.Models.PageModel;
using System;
using System.Collections.Generic;
using System.Text;
using KeepAccounts.Models.Enums.UserInfo;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 用户登录
    /// </summary>
    public class UserInfoListQueryModel : BasePageQuery<UserInfoEntity>
    {

        /// <summary>
        /// 用户名
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }


        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsAdmin { get; set; }


      
        /// <summary>
        /// 公司id
        /// </summary>
        public string CompanyId { get; set; }


    }
    public class ResumeListQueryModel : BasePageQuery<UserInfoEntity>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 性别 不限 = 0,  男 = 1,   女 = 2 
        /// </summary>
        public int Sex { get; set; }
        ///// <summary>
        ///// 职位名称
        ///// </summary>
        //public string JobName { get; set; }

        /// <summary>
        /// 薪资范围
        /// </summary>
        public string SalaryRange { get; set; }

        /// <summary>
        /// 地区
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }
        /// <summary>
        /// 参加工作时间
        /// </summary>
        public int MinWorkHour { get; set; }
        /// <summary>
        /// 参加工作时间
        /// </summary>
        public int MaxWorkHour { get; set; }
        /// <summary>
        /// 最小年龄
        /// </summary>
        public int MinAge { get; set; }
        /// <summary>
        /// 最大年龄
        /// </summary>
        public int MaxAge { get; set; }
        /// <summary>
        /// 求职状态
        /// </summary>
        public string JobState { get; set; }
    }
}
