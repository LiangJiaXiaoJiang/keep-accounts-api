﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using KeepAccounts.Models.Enums.UserInfo;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 第三方授权登陆
    /// </summary>
    public class OAuthLoginQueryModel
    {

        /// <summary>
        /// 第三方平台Code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// 授权登陆类型
        /// </summary>
        public OAuthTypeEnum OAuthType { get; set; }

        /// <summary>
        /// 用户数据 平台不一样 的格式不一样
        /// </summary>
        public WhatUserInfo UserInfo { get; set; }

    }
}
