﻿using KeepAccounts.Models.Base;
using KeepAccounts.Models.Enums.UserInfo;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text; 

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 用户
    /// </summary>
    [SqlSugar.SugarTable("UserInfo")]
    public class UserInfoEntity : BaseEntity
    {
        
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 支付密码
        /// </summary>
        public string PayPassword { get; set; }

        /// <summary>
        /// 是否管理员 0:公司hr普通账号 1：总后台账号 2：公司管理账号
        /// </summary>
        public int IsAdmin { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }



        /// <summary>
        /// 头像
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public string HeaderImage { get; set; }


        /// <summary>
        /// 性别 1男  2女
        /// </summary>
        public SexEnum Sex { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BirthTime { get; set; }

        /// <summary>
        /// 是否Vip
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public bool IsVip { get; set; }

        /// <summary>
        /// Vip到期时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime VipExpirationDate { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public bool IsDisable { get; set; }
        /// <summary>
        /// 父级Id
        /// </summary>
        /// 
        [SqlSugar.SugarColumn(IsNullable = true)]
        public Guid ParentId { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        /// 
        [SqlSugar.SugarColumn(IsNullable = true)]
        public decimal Integral { get; set; }

        /// <summary>
        /// 微信openId
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public string WeChatOpenId { get; set; }

        /// <summary>
        /// 微信UnionId
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public string WeChatUnionId { get; set; }


        /// <summary>
        /// 最近登录时间
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// 兴趣爱好
        /// </summary>
        [SqlSugar.SugarColumn(IsNullable = true)]
        public string Interests { get; set; }


        /// <summary>
        /// 国家
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
    }
}
