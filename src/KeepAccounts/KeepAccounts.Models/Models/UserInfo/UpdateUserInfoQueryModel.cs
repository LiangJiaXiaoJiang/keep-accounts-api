﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 修改个人信息请求参数
    /// </summary>
    public class UpdateUserInfoQueryModel
    {

        /// <summary>
        /// 头像
        /// </summary>
        public string HeaderImage { get; set; }


        /// <summary>
        /// 用户昵称
        /// </summary>
        public string UserName { get; set; }

    }
}
