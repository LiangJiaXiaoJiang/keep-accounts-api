﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 找回密码
    /// </summary>
    public class FindPasswordQueryModel
    {

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }


        /// <summary>
        /// 验证码
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
    }
}
