﻿using KeepAccounts.Models.Enums.UserInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 登录
    /// </summary>
    public class LoginQueryModel
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 手机验证码
        /// </summary>
        public string PhoneCode { get; set; }


        /// <summary>
        /// 登录类型
        /// </summary>
        public LoginTypeEnum LoginType { get; set; }


        /// <summary>
        /// 登陆身份 1 求职者 2 Boss_HR  3 后台管理员
        /// </summary>
        public int UserIdentity { get; set; }

    }

}
