﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.UserInfo
{
    /// <summary>
    /// 重置密码
    /// </summary>
    public class ResetPasswordQueryModel
    { 
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid Id { get; set; }


        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }




    }
}
