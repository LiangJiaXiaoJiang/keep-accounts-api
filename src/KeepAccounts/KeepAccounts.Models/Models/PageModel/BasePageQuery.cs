﻿using KeepAccounts.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.Json.Serialization;

namespace KeepAccounts.Models.PageModel
{
    /// <summary>
    /// 分页查询基础实体
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BasePageQuery<TEntity> where TEntity : BaseEntity, new()
    {
        public BasePageQuery()
        {
            PageIndex = 1;
            PageSize = 20;
            //OrderString = string.Empty;
            OrderString = " Sort desc ,CreateTime desc  ";//默认时间排序
        }
        /// <summary>
        /// 页码
        /// </summary>
        public virtual int PageIndex { get; set; }
        /// <summary>
        /// 分页数量
        /// </summary>
        public virtual int PageSize { get; set; }
        [JsonIgnore]
        public virtual Expression<Func<TEntity, bool>> WhereLambda { get; set; } = null;

        [JsonIgnore]
        public virtual string OrderString { get; set; }

    }
}