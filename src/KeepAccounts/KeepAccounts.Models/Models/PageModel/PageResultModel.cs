﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeepAccounts.Models.PageModel
{
    /// <summary>
    /// 分页返回数据实体信息
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public class PageResultModel<TModel>
    {
        public PageResultModel()
        {
            ServerTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        private int pages = 0;
        private int pageSize = 0;
        /// <summary>
        /// 数据集合
        /// </summary>
        public List<TModel> List { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 页码数量
        /// </summary>
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = value;
            }
        }
        /// <summary>
        /// 总页数
        /// </summary>
        public int Pages
        {
            get
            {
                if (pageSize > 0 && Count > 0)
                {
                    pages = (int)Math.Ceiling(((decimal)Count / PageSize));
                }
                return pages;
            }
        }

        /// <summary>
        /// 当前服务器时间
        /// </summary>
        public string ServerTime { get; set; }

        public bool Any()
        {
            return List != null && List.Any();
        }

        public static PageResultModel<TModel> Default()
        {
            return new PageResultModel<TModel>
            {
                Count = 0,
                List = new List<TModel> { },
            };
        }

        public static implicit operator PageResultModel<TModel>(bool v)
        {
            throw new NotImplementedException();
        }
    }


}
