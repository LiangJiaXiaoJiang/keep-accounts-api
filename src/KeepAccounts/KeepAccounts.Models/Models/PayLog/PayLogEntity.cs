﻿using KeepAccounts.Models.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Models.PayLog
{
    /// <summary>
    /// 订单支付实体
    /// </summary>
    [SugarTable("PayLog")]
    public class PayLogEntity : BaseEntity 
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        ///  订单Id
        /// </summary>
        public Guid OrderId { get; set; }
        /// <summary>
        /// 支付订单Id 第三方订单号 （支付宝 微信）
        /// </summary> 
        public string OrderPayId { get; set; }
        /// <summary>
        /// 产品码
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 总金额
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 订单标题
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 超时时间
        /// </summary>
        public string TimeExpire { get; set; }
        /// <summary>
        /// UserIP
        /// </summary>
        public string UserIP { get; set; }

        /// <summary>
        /// NotifyUrl
        /// </summary>
        public string NotifyUrl { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public string Data { get; set; }
        /// <summary>
        /// Result
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 支付类型
        /// </summary>
        public int PayType { get; set; }

    }
}
