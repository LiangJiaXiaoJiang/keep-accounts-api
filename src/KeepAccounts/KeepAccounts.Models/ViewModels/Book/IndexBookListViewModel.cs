﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.ViewModels.Book
{
    /// <summary>
    /// 首页数据
    /// </summary>
    public class IndexBookListViewModel
    {

        public IndexBookListViewModel() {
            this.DayBookList = new List<DayBook>(); 
            this.TotalExpend = 0;
            this.TotalIncome = 0;
            this.YearMonth = DateTime.Now.ToString("yyyy年MM月"); 
         
        }
        /// <summary>
        /// 总收入
        /// </summary>
        public decimal TotalIncome { get; set; }
        /// <summary>
        /// 总支出
        /// </summary>
        public decimal TotalExpend { get; set; }
        /// <summary>
        /// 结余
        /// </summary>
        public decimal Balance { get { return this.TotalIncome - this.TotalExpend; } }
        /// <summary>
        /// 月份
        /// </summary>
        public string YearMonth { get; set; }

        /// <summary>
        /// 列表
        /// </summary>
        public List<DayBook> DayBookList { get; set; }

    }
}
