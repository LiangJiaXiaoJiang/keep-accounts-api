﻿using KeepAccounts.Models.Enums.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.ViewModels.Book
{
   /// <summary>
   /// 分类图标
   /// </summary>
    public class BookListViewModel
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 图标名称
        /// </summary>
        public string ClassIcon { get; set; }


        /// <summary>
        /// 背景颜色
        /// </summary>
        public string ClassBg { get; set; }

        /// <summary>
        /// 账单类型
        /// </summary>
        public BillTypeEnum BillType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal AmountOfMoney { get; set; }


        /// <summary>
        /// 时间
        /// </summary>
        public DateTime ConsumptionTime { get; set; }


    }
}
