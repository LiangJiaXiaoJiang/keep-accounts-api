﻿using KeepAccounts.Models.Enums.Book;
using System.Collections.Generic;
using System.Linq;

namespace KeepAccounts.Models.ViewModels.Book
{
    public class DayBook
    {


        /// <summary>
        /// 收入
        /// </summary>
        public decimal Income { get {

                return this.BookList.Where(s => s.BillType == BillTypeEnum.收入).Sum(s => s.AmountOfMoney);
            } }
        /// <summary>
        /// 支出
        /// </summary>
        public decimal Expend { get {
                return this.BookList.Where(s => s.BillType == BillTypeEnum.支出).Sum(s => s.AmountOfMoney);
            }}
        /// <summary>
        /// 
        /// </summary>
        public string Day { get; set; }
        /// <summary>
        /// 数据列表
        /// </summary>
        public List<BookListViewModel> BookList { get; set; }
    }
}