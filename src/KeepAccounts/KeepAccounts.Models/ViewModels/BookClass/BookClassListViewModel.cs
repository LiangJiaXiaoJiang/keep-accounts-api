﻿using KeepAccounts.Models.Enums.Book;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.ViewModels.BookClass
{
   /// <summary>
   /// 分类图标
   /// </summary>
    public class BookClassListViewModel
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 图标名称
        /// </summary>
        public string IconName { get; set; }


        /// <summary>
        /// 背景颜色
        /// </summary>
        public string BgColor { get; set; }

        /// <summary>
        /// 账单类型
        /// </summary>
        public BillTypeEnum BillType { get; set; }


    }
}
