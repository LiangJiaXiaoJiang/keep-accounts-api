﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
   public class SelectCountViewModel
    {
        /// <summary>
        /// 数量 count(*)
        /// </summary>
        public int Num { get; set; }
        /// <summary>
        /// 行数
        /// </summary>
        public int RowIndex { get; set; }
    }
}
