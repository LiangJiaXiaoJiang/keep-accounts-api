﻿using System;
using System.Collections.Generic;
using System.Text;
using KeepAccounts.Models.Enums.UserInfo;

namespace KeepAccounts.Models.ViewModels.UserInfo
{
    /// <summary>
    /// 获取用户信息返回数据
    /// </summary>
    public class UserInfoResultViewModel
    {
        /// <summary>
        /// 是否管理员 0:公司hr普通账号 1：总后台账号 2：公司管理账号
        /// </summary>
        public int IsAdmin { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }


        /// <summary>
        /// 头像
        /// </summary>
        public string HeaderImage { get; set; }


        /// <summary>
        /// 积分
        /// </summary>
        public decimal Integral { get; set; }


        /// <summary>
        /// 性别
        /// </summary>
        public SexEnum Sex { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }


        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BirthTime { get; set; }


        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get { var age = DateTime.Now.Year - BirthTime.Year; 
                return  age ; } }
                //return (age > 0 && age <= 100) ? age : 0; }}


/// <summary>
/// 参加工作时间
/// </summary>
public DateTime WorkHour { get; set; }

        /// <summary>
        /// 已经工作多长时间了
        /// </summary>
        public int Worked
        {
            get
            {
                var worked = DateTime.Now.Year - this.WorkHour.Year;
                return worked;
                //return (worked > 0 && worked <= 100) ? worked : 0;
            }
        }


        /// <summary>
        ///推广数量
        /// </summary>
        public int ExpandedCount { get; set; }


        /// <summary>
        /// 是否Vip
        /// </summary>
        public bool IsVip { get; set; }


        /// <summary>
        /// Vip到期时间
        /// </summary>
        public DateTime? VipExpirationDate { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public Guid CompanyId { get; set; }
    }
}
