﻿using System;
using System.Collections.Generic;
using System.Text;
using KeepAccounts.Models.Enums.UserInfo;

namespace KeepAccounts.Models.ViewModels.UserInfo
{
    /// <summary>
    /// 登录返回
    /// </summary>
    public class LoginResultViewModel
    {

        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 接口请求token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 是否设置密码 true 没有设置密码  false 已经设置密码
        /// </summary>
        public bool IsNeedPassword { get; set; }


        /// <summary>
        /// 是否管理员 
        /// </summary>
        public int IsAdmin { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public List<int> Role { get; set; }

    }
}
