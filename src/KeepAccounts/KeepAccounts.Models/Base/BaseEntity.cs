﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Base
{
    /// <summary>
    /// 基础类型
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// id
        /// </summary>
        [SugarColumn(IsPrimaryKey = true)]
        public Guid ID { get; set; }

        protected BaseEntity()
        {
            this.ID = Guid.NewGuid();
            this.IsDeleted = false;
            this.CreateTime = DateTime.Now;
            this.UpdateTime = DateTime.Now;
            this.Sort = 0;
        }



        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDeleted { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// 创建人
        /// </summary>
        public Guid CreateUserID { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }


        /// <summary>
        /// 修改人
        /// </summary>
        public Guid UpdateUserID { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int Sort { get; set; }
        

    }
}
