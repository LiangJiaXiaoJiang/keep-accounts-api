﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Base
{
    /// <summary>
    /// 关联用户Base实体
    /// </summary>
    public class UserBaseEntity:BaseEntity
    {

        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }


    }
}
