﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Base
{
    public class BaseResultModel
    {
        /// <summary>
        /// 接口返回的状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 接口返回的消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 多返回的参数
        /// </summary>
        public string ResultId { get; set; }
    }
}
