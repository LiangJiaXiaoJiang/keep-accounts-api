﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.Pay
{



    /// <summary>
    /// 支付方式
    ///  支付宝 = 1,
    ///  微信 = 2,  
    /// </summary>
    public enum PayTypeEnum
    {
        支付宝 = 1,
        微信 = 2,
    }
}
