﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.Pay
{
    /// <summary>
    /// 支付状态
    /// </summary>
    public enum OrderStatus
    {

        /// <summary>
        /// 未支付
        /// </summary>
        未支付 = 0,
        /// <summary>
        /// 已支付
        /// </summary>
        已支付 = 1

    }
}
