﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.Book
{
    /// <summary>
    /// 账单类型 1支出 2收入
    /// </summary>
    public enum BillTypeEnum
    {
        支出 = 1,
        收入 = 2 

    }

}
