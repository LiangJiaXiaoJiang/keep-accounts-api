﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    /// 个人信息编辑分类
    /// </summary>
    public enum SelfInfoEnum
    {
        /// <summary>
        /// 自我评价
        /// </summary>
        SelfEvaluate = 0,
        /// <summary>
        /// 荣誉奖项
        /// </summary>
        Honor = 1,
        /// <summary>
        /// 兴趣爱好
        /// </summary>
        Interests = 2,

    }
}
