﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    /// 性别   未知 = 0,  男 = 1,   女 = 2
    /// </summary>
    public enum SexEnum
    {
        /// <summary>
        /// 未知
        /// </summary>
        未知 = 0,
        /// <summary>
        /// 男
        /// </summary>
        男 = 1,

        /// <summary>
        /// 女
        /// </summary>
        女 = 2


    }
}
