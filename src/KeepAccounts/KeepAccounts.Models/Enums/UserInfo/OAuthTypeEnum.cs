﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    /// 授权类型
    /// </summary>
    public enum OAuthTypeEnum
    {
        /// <summary>
        /// 微信
        /// </summary>
        WeChat = 1,
        /// <summary>
        /// 支付宝
        /// </summary>
        AliPay = 2,
        /// <summary>
        /// QQ
        /// </summary>
        QQ = 3,
        /// <summary>
        /// 微博
        /// </summary>
        Sina = 4,


    }
}
