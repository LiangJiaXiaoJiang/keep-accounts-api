﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    /// 登录类型 密码登录1   验证码登录2
    /// </summary>
    public enum LoginTypeEnum
    {
        /// <summary>
        /// 密码登录
        /// </summary>
        密码登录 = 1,

        /// <summary>
        /// 验证码登录
        /// </summary>
        验证码登录 = 2

    }
}
