﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    ///     设置密码 = 1,  修改密码 = 2,
    /// </summary>
    public enum UpdatePasswordTypeEnum
    {

        /// <summary>
        /// 设置密码
        /// </summary>
        设置密码 = 1,

        /// <summary>
        /// 修改密码
        /// </summary>
        修改密码 = 2,

        ///// <summary>
        ///// 找回密码
        ///// </summary>
        //找回密码 = 3,

    }
}
