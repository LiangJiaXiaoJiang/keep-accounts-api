﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace KeepAccounts.Models.Enums.UserInfo
{
    /// <summary>
    /// 职位状态
    /// </summary>
    public enum JobStateEnum
    {
        /// <summary>
        /// 立即到岗
        /// </summary>
        立即到岗 = 1,
        /// <summary>
        /// 一个月到岗
        /// </summary>
        一个月到岗 = 2,
        /// <summary>
        /// 三个月到岗
        /// </summary>
        三个月到岗 = 3,
        /// <summary>
        /// 六个月到岗
        /// </summary>
        六个月到岗 = 4
    }
    
}
