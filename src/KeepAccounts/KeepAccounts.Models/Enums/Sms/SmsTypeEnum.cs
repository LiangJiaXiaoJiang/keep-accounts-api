﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Enums.Sms
{
    /// <summary>
    /// 短信类型
    /// 验证码 = 1,
    /// 通知短信 = 2,
    /// 推广短信 = 3
    /// </summary>
    public enum SmsTypeEnum
    {
        验证码 = 1,
        通知短信 = 2,
        推广短信 = 3
    }
}
