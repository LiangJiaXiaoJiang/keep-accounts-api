﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models
{
    public class ApiException : Exception
    {
        public int ErrorCode { get; }

        public ApiException()
        {

        }

        public ApiException(string message)
            : base(message)
        {

        }

        public ApiException(string message, int code)
            : base(message)
        {
            ErrorCode = code;
        }

        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public static void Throw(string message)
        {
            throw new ApiException(message);
        }
    }
}
