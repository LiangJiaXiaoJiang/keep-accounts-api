﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models
{
    /// <summary>
    /// 返回数据
    /// </summary>
    public class ResponseModel
    {

        /// <summary>
        /// 请求返回失败
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ResponseModel Error(string msg = "请求失败")
        {
            return new ResponseModel { Status = 0, Msg = msg , Data=new object() };
        }


    
        /// <summary>
        /// 请求返回失败
        /// </summary>  
        /// <param name="msg"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ResponseModel Error(string msg = "请求失败",object data=null)
        {
            return new ResponseModel { Status = 0, Msg = msg, Data = data };
        }
        /// <summary>
        /// 请求成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ResponseModel Success(object data = null, string msg = "请求成功")
        {
            return new ResponseModel { Status = 1, Msg = msg, Data = data };
        }

        /// <summary>
        /// 请求成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ResponseModel Success<T>(T data, string msg = "请求成功")
        {
            return new ResponseModel { Status = 1, Msg = msg, Data = data };
        }


        /// <summary>
        /// 返回状态 0 失败 1 成功
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }


        /// <summary>
        /// 返回数据
        /// </summary>
        public object Data { get; set; }
    }
}
