﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepAccounts.Models.Swagger
{
    public static class Grouping
    {
        /// <summary>
        /// 通用接口
        /// </summary>
        public const string V1   = "通用接口";

        /// <summary>
        /// 后台接口
        /// </summary>
        public const string V2  = "后台接口";
         
        /// <summary>
        /// App接口
        /// </summary>
        public const string V3  = "APP接口";

        

    }
}
